@extends('layouts.master')
@section('content')
     <!-- Content Header (Page header) -->
 <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pertanyaan</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          @forelse ($pertanyaan as $p)
          <div class="col-md-4">
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                <span class="username"><a href="pertanyaan/{{$p->id}}">Orang</a></span>
                  <span class="description">{{$p->created_at}}</span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <a class="btn btn-tool" href="pertanyaan/{{$p->id}}/edit">Edit</a>
                  <form action="{{route('pertanyaan.destroy',[$p->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-tool text-danger">Delete</button>               
                  </form>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                {{-- <img class="img-fluid pad" src="../dist/img/photo2.png" alt="Photo"> --}}
                <h3><strong>{{$p->judul}}</strong></h3>
                <p>{{$p->isi}}</p>
                <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-thumbs-down"></i> Dislike</button>
              </div>
              <!-- /.card-body -->
              {{-- <div class="card-footer card-comments">
                <div class="card-comment">
                  <!-- User image -->
                  <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">

                  <div class="comment-text">
                    <span class="username">
                      Maria Gonzales
                      <span class="text-muted float-right">8:03 PM Today</span>
                    </span><!-- /.username -->
                    It is a long established fact that a reader will be distracted
                    by the readable content of a page when looking at its layout.
                  </div>
                  <!-- /.comment-text -->
                </div>
                <!-- /.card-comment -->
                <div class="card-comment">
                  <!-- User image -->
                  <img class="img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="User Image">

                  <div class="comment-text">
                    <span class="username">
                      Luna Stark
                      <span class="text-muted float-right">8:03 PM Today</span>
                    </span><!-- /.username -->
                    It is a long established fact that a reader will be distracted
                    by the readable content of a page when looking at its layout.
                  </div>
                  <!-- /.comment-text -->
                </div>
                <!-- /.card-comment -->
              </div> --}}
              <!-- /.card-footer -->
              <div class="card-footer">
                <form action="#" method="post">
                  <img class="img-fluid img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push">
                    <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
                  </div>
                </form>
              </div>
              <!-- /.card-footer -->
            </div>
            {{-- <div class="card">
              <div class="card-body">
                <h5 class="card-title">{{$p->judul}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$p->created_at}}</h6>
                <p class="card-text">{{$p->isi}}</p>
                <a href="#" class="card-link">Edit</a>
                <a href="#" class="card-link text-danger">Hapus</a>
              </div>
            </div> --}}
          </div>
          @empty
              <h1>Belum Ada Pertanyaan</h1>
          @endforelse
        </div>
    </div>
</section>
@endsection