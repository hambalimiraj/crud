@extends('layouts.master')
@section('content')
     <!-- Content Header (Page header) -->
 <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          
          
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                <span class="username"><a href="pertanyaan/{{$pertanyaan->id}}">Orang</a></span>
                  <span class="description">{{$pertanyaan->created_at}}</span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <a class="btn btn-tool" href="pertanyaan/{{$pertanyaan->id}}/edit">Edit</a>
                  <form action="{{route('pertanyaan.destroy',[$pertanyaan->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-tool text-danger">Delete</button>               
                  </form>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                {{-- <img class="img-fluid pad" src="../dist/img/photo2.png" alt="Photo"> --}}
                <h3><strong>{{$pertanyaan->judul}}</strong></h3>
                <p>{{$pertanyaan->isi}}</p>
                <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-thumbs-down"></i> Dislike</button>
              </div>
             
              <div class="card-footer">
                <form action="#" method="post">
                  <img class="img-fluid img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push">
                    <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
                  </div>
                </form>
              </div>
              <!-- /.card-footer -->
            </div>
           
          </div>
          
        </div>
    </div>
</section>
@endsection